## Git Advanced

This repo contains some examples of Git advanced features.

1. Git internals 
  - [Blobs](/gadabout/git-advanced/src/master/internals-blobs.md)
  - [Trees](/gadabout/git-advanced/src/master/internals-trees.md)
  - [Commits](/gadabout/git-advanced/src/master/internals-commits.md)
2. 

--

Please don't drink and git.

