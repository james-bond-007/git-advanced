# Cleaning

```
git clean -n 
```

Dry-run.

```
git clean -f 
```

Force clean.

```
git clean -x 
```

Remove all untracked (including ignored) files.